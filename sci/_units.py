from pint import UnitRegistry

# Set up pint for unit handling
# http://pint.readthedocs.io/en/latest/tutorial.html#using-pint-in-your-projects
units = UnitRegistry()
Q_ = units.Quantity 