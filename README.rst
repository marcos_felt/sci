sci
===

.. image:: https://img.shields.io/pypi/v/sci.svg
    :target: https://pypi.python.org/pypi/sci
    :alt: Latest PyPI version

.. image:: https://gitlab.com/marcos_felt/sci/badges/master/pipeline.svg
   :target: https://gitlab.com/marcos_felt/sci/commits/master
   :alt: Gitlab CI/CD Pipeline

Design, automate and share any science experiment.

Usage
-----

Installation
------------

Requirements
^^^^^^^^^^^^

Compatibility
-------------

Licence
-------

Authors
-------

`sci` was written by `scici <sci@sci.ci>`_.
