from sci.protocols import step, Protocol
from sci.instruments import SyringePump
from sci.refs import Syringe
from sci import units

liquid = Syringe('0.5M NaOH', liquid_volume=10.0 * units.milliliters)
pump = SyringePump(refs=[liquid])

@step(description='start reaction', inputs = [], instruments=[('pump',pump)], outputs=[])
def test_step(pump):
    return "Hello"

protooo = Protocol(name='Hello World',description='A sample protocol', steps=[test_step],
                   driver_db='test_instrumentdb.json')

protooo()